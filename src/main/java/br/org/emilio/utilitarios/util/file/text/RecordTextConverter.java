package br.org.emilio.utilitarios.util.file.text;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.org.emilio.utilitarios.util.file.text.annotation.TextField;
import br.org.emilio.utilitarios.util.file.text.annotation.TextRecord;
import br.org.emilio.utilitarios.util.file.text.dto.ReadedRecord;
import br.org.emilio.utilitarios.util.file.text.dto.ReadedRecordError;
import br.org.emilio.utilitarios.util.file.text.exception.TextFieldException;

public class RecordTextConverter {

	public <T> ReadedRecord toReadedRecord(String text, Class<T> clazz) {
		T objDados;
		try {
			objDados = clazz.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}

		int posicao = 0;
		List<ReadedRecordError> errorList = new ArrayList<ReadedRecordError>();
		
		if (clazz.isAnnotationPresent(TextRecord.class)) {
			TextRecord annot = clazz.getAnnotation(TextRecord.class);
			int recordLength = annot.length();
			
			if (recordLength != 0 && recordLength != text.length()) {
				ReadedRecordError error = new ReadedRecordError();
				error.setColumn(posicao + 1);
				error.setField(null);
				error.setValue(null);
				error.setDescription("Largura da linha " + text.length() + " não confere com o especificado: " + recordLength);
				
				errorList.add(error);
			}
		}
		
		for (Field field : clazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(TextField.class)) {
				TextField annot = field.getAnnotation(TextField.class);
				Class<?> type = field.getType();
				Method setter = this.getSetter(clazz, field, type);
				int length = annot.length();
				int decimals = annot.decimals();
				String dateFormat = annot.dateFormat();
				String trueValue = annot.trueValue();
				String falseValue = annot.falseValue();

				String value = this.getValue(text, posicao, length);
				
				try {
					if (type == String.class) {
						objDados = this.tratarString(objDados, value, setter);
					} else if (this.isNumberType(type)) {
						objDados = this.tratarNumber(objDados, value, field, setter, type, decimals);
					} else if (type == Date.class) {
						objDados = this.tratarDate(objDados, value, field, setter, dateFormat);
					} else if (type == boolean.class) {
						objDados = this.tratarBoolean(objDados, value, field, setter, trueValue, falseValue);
					} else {
						throw new RuntimeException(this.messageErrorType(field));
					}
				} catch(TextFieldException e) {
					ReadedRecordError error = new ReadedRecordError();
					error.setColumn(posicao + 1);
					error.setField(field.getName());
					error.setValue(value);
					error.setDescription(e.getMessage());
					
					errorList.add(error);
				}
				
				posicao += length;
			}
		}
		
		ReadedRecord record = new ReadedRecord();
		record.setSuccess(errorList.isEmpty() ? true : false);
		record.setErrors(errorList);
		record.setRecord(objDados);
		record.setText(text);
		
		return record;
	}

	@SuppressWarnings("unchecked")
	public <T> T toRecord(String text, Class<T> clazz) {
		return (T) this.toReadedRecord(text, clazz).getRecord();
	}
	
	private boolean isNumberType(Class<?> type) {
		Class<?>[] types = {
			byte.class, short.class, int.class, long.class,
			float.class, double.class
		};

		for (Class<?> subType : types) {
			if (type == subType) {
				return true;
			}
		}
		
		return false;
	}
	
	private String getValue(String text, int posicao, int tamanho) {
		if (text == null) {
			return "";
		}
		
		int posicaoInicial = posicao;
		if (posicaoInicial > text.length()) {
			posicaoInicial = text.length();
		}
		
		int posicaoFinal = posicaoInicial + tamanho;
		if (posicaoFinal > text.length()) {
			posicaoFinal = text.length();
		}
		
		return text.substring(posicaoInicial, posicaoFinal);
	}
	
	private Method getSetter(Class<?> clazz, Field field, Class<?> type) {
		String nomeCampo = field.getName();
		String nomeMetodo = "set" 
			+ nomeCampo.substring(0, 1).toUpperCase()
			+ nomeCampo.substring(1);
		Method metodo;
		try {
			 metodo = clazz.getMethod(nomeMetodo, type);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		
		return metodo;
	}
	
	private String charRepeat(char chr, int tamanho) {
		String str = "";
		
		for ( int i = 0 ; i < tamanho ; i++ ) {
			str += chr;
		}
		
		return str;
	}
	
	private <T> T tratarString(T objDados, String value, Method setter) {
		if (objDados == null) {
			return null;
		}
		
		try {
			setter.invoke(objDados, value.trim());
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		
		return objDados;
	}

	private <T> T tratarNumber(T objDados, String value, Field field, Method setter, Class<?> type, int tamanhoDecimal) {
		if (objDados == null) {
			return null;
		}
		
		value = value.replace('.', '^');
		value = value.replace(',', '^');
		value = value.replace('E', '&');
		value = value.replace('e', '&');
		value = value.replace('-', '_');
		value = value.replace('+', 'x');
		
		try {
			int dividir = Integer.parseInt("1" + this.charRepeat('0', tamanhoDecimal), 10);
			double number;
			if (value.trim().equals("")) {
				number = 0;
			} else {
				number = new DecimalFormat("0").parse(value).doubleValue() / dividir;
			}

			if (type == int.class) {
				setter.invoke(objDados, (int) number);
			} else if (type == long.class) {
				setter.invoke(objDados, (long) number);
			} else if (type == short.class) {
				setter.invoke(objDados, (short) number);
			} else if (type == byte.class) {
				setter.invoke(objDados, (byte) number);
			} else if (type == float.class) {
				setter.invoke(objDados, (float) number);
			} else {
				setter.invoke(objDados, number);
			}
		} catch (ParseException e) {
			throw new TextFieldException(this.messageErrorValue(field, value));
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		
		return objDados;
	}

	private <T> T tratarDate(T objDados, String value, Field field, Method setter, String dateFormat) {
		if (objDados == null) {
			return null;
		}
		
		try {
			Date date;
			if (value.trim().equals("")) {
				date = null;
			} else {
				SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
				sdf.setLenient(false);
				date = sdf.parse(value);
			}
			
			setter.invoke(objDados, date);
		} catch (ParseException e) {
			throw new TextFieldException(this.messageErrorValue(field, value));
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		
		return objDados;
	}

	private <T> T tratarBoolean(T objDados, String value, Field field, Method setter, String trueValue, String falseValue) {
		if (objDados == null) {
			return null;
		}
		
		try {
			boolean bool;
			if (value.trim().equals("")) {
				bool = false;
			} else if (value.equals(trueValue)) {
				bool = true;
			} else if (value.equals(falseValue)) {
				bool = false;
			} else {
				throw new TextFieldException(this.messageErrorValue(field, value));
			}
			
			setter.invoke(objDados, bool);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		
		return objDados;
	}
	
	private String messageErrorType(Field field) {
		return "Tipo nao permitido para conversao em " + field.getName();
	}
	
	private String messageErrorValue(Field field, String value) {
		return "Valor nao permitido para " + field.getName() + ": '" + value + "'";
	}
	
	public String toText(Object object) {
		String textDados = "";
		
		for (Field field : object.getClass().getDeclaredFields()) {
			if (field.isAnnotationPresent(TextField.class)) {
				TextField annot = field.getAnnotation(TextField.class);
				Method getter = this.getGetter(object.getClass(), field);
				Class<?> type = field.getType();
				int length = annot.length();
				int decimals = annot.decimals();
				String dateFormat = annot.dateFormat();
				String trueValue = annot.trueValue();
				String falseValue = annot.falseValue();

				Object value = this.getValue(object, getter);
				
				String textValue;
				if (type == String.class) {
					textValue = this.tratarString(value, length);
				} else if (this.isNumberType(type)) {
					textValue = this.tratarNumber(value, length, type, decimals);
				} else if (type == Date.class) {
					textValue = this.tratarDate(value, length, dateFormat);
				} else if (type == boolean.class) {
					textValue = this.tratarBoolean(value, length, trueValue, falseValue);
				} else {
					throw new RuntimeException(this.messageErrorType(field));
				}
				
				textDados += textValue;
			}
		}
		
		return textDados;
	}
	
	private Object getValue(Object object, Method getter) {
		try {
			return getter.invoke(object);
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private String completarComSpaces(String value, int length) {
		if (value.length() > length) {
			value = value.substring(0, length);
		} else if (value.length() < length) {
			value += this.charRepeat(' ', length - value.length());
		}
		
		return value;
	}

	private String completarComZeros(String value, int length) {
		if (value.length() > length) {
			throw new RuntimeException("Largura do valor maior que o permitido: " + value.length() + ", esperado: " + length);
		} else if (value.length() < length) {
			value = this.charRepeat('0', length - value.length()) + value;
		}
		
		return value;
	}
	
	private Method getGetter(Class<?> clazz, Field field) {
		String nomeCampo = field.getName();
		String nomeMetodo = (field.getType() == boolean.class ? "is" : "get") 
				+ nomeCampo.substring(0, 1).toUpperCase()
				+ nomeCampo.substring(1);
		Method metodo;
		try {
			 metodo = clazz.getMethod(nomeMetodo);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		
		return metodo;
	}
	
	private String tratarString(Object value, int length) {
		String string;
		if (value == null) {
			string = "";
		} else {
			Charset cs = Charset.forName("ISO-8859-1");
			string = new String(((String) value).getBytes(cs), cs);
		}
		
		return this.completarComSpaces(string, length);
	}
	
	private String tratarNumber(Object value, int length, Class<?> type, int tamanhoDecimal) {
		double dbl;
		if (type == int.class) {
			dbl = (int) value;
		} else if (type == long.class) {
			dbl = (long) value;
		} else if (type == short.class) {
			dbl = (short) value;
		} else if (type == byte.class) {
			dbl = (byte) value;
		} else if (type == float.class) {
			dbl = (float) value;
		} else {
			dbl = (double) value;
		}
		if (dbl < 0) {
			throw new RuntimeException("Não é permitido valor negativo");
		}
		
		int multiplicar = Integer.parseInt("1" + this.charRepeat('0', tamanhoDecimal), 10);
		String number = new DecimalFormat("0").format(dbl * multiplicar);
		return this.completarComZeros(number, length);
	}
	
	private String tratarDate(Object value, int length, String dateFormat) {
		String date;
		if (value == null) {
			date = "";
		} else {
			date = new SimpleDateFormat(dateFormat).format((Date) value);
		}
		
		return this.completarComSpaces(date, length);
	}
	
	private String tratarBoolean(Object value, int length, String trueValue, String falseValue) {
		String bool;
		if ((boolean) value) {
			bool = trueValue;
		} else {
			bool = falseValue;
		}
		
		return this.completarComSpaces(bool, length);
	}
	
}
