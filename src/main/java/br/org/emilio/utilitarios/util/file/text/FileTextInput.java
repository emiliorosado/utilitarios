package br.org.emilio.utilitarios.util.file.text;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import br.org.emilio.utilitarios.util.file.text.dto.ReadedRecord;

public class FileTextInput {
	
	private InputStream inputStream;
	private InputStreamReader inputStreamReader;
	private BufferedReader bufferedReader;
	private RecordTextConverter converter;
	private Class<?> clazz;

	public FileTextInput(String pathname, Class<?> clazz) {
		try {
			File f = new File(pathname);
			this.inputStream = new FileInputStream(f);
			this.inputStreamReader = new InputStreamReader(inputStream);
			this.bufferedReader = new BufferedReader(inputStreamReader);
			this.converter = new RecordTextConverter();
			this.clazz = clazz;
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public FileTextInput(String pathname) {
		this(pathname, null);
	}
	
	public void close() {
		try {
			this.bufferedReader.close();
			this.inputStreamReader.close();
			this.inputStream.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public String readAsText() {
		try {
			return this.bufferedReader.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public ReadedRecord read(Class<?> clazz) {
		String line = this.readAsText();
		
		if (line == null) {
			return null;
		}
		
		return this.converter.toReadedRecord(line, clazz);
	}
	
	public ReadedRecord read() {
		if (this.clazz == null) {
			throw new RuntimeException("Classe nao definida para leitura");
		}
		
		return this.read(this.clazz);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T readAsRecord(Class<T> clazz) {
		ReadedRecord input = this.read(clazz);
		
		if (input == null) {
			return null;
		}
		
		return (T) input.getRecord();
	}
	
	public <T> T readAsRecord() {
		@SuppressWarnings("unchecked")
		Class<T> clazz = (Class<T>) this.clazz;
		
		if (clazz == null) {
			throw new RuntimeException("Classe nao definida para leitura");
		}
		
		return (T) this.readAsRecord(clazz);
	}
	
}
