package br.org.emilio.utilitarios.util.file.text.exception;

public class TextFieldException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public TextFieldException(String message) {
		super(message);
	}
	
}
