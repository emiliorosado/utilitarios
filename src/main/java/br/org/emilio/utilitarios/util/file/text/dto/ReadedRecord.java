package br.org.emilio.utilitarios.util.file.text.dto;

import java.util.List;

import lombok.Data;

@Data
public class ReadedRecord {

	private boolean success;
	private List<ReadedRecordError> errors;
	private Object record;
	private String text;
	
}
