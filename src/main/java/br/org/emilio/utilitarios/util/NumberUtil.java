package br.org.emilio.utilitarios.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

public class NumberUtil {

	private NumberUtil() {
	}
	
	public static boolean empty(Double num) {
		if (num == null || num.doubleValue() == 0.0) {
			return true;
		}
		
		return false;
	}
	
	public static boolean empty(String num) {
		if (null == null || "".equals(num.trim()) || parse(num, "0.0") == 0.0) {
			return true;
		}
		
		return false;
	}
	
	private static DecimalFormat getDecimalFormat(String fmt, char groupingSeparator, char decimalSeparator) {
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator(groupingSeparator);
		symbols.setDecimalSeparator(decimalSeparator);
		return new DecimalFormat(fmt, symbols);
	}

	public static String format(double num, String fmt, char groupingSeparator, char decimalSeparator) {
		DecimalFormat df = getDecimalFormat(fmt, groupingSeparator, decimalSeparator);
		return df.format(num);
	}
	
	public static String format(double num, String fmt) {
		return format(num, fmt, '.', ',');
	}

	public static double parse(String num, String fmt, char groupingSeparator, char decimalSeparator) {
		DecimalFormat df = getDecimalFormat(fmt, groupingSeparator, decimalSeparator);
		double number;
		
		try {
			number = df.parse(num).doubleValue();
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		
		return number;
	}
	
	public static double parse(String num, String fmt) {
		return parse(num, fmt, '.', ',');
	}
	
	public static boolean valid(String num, String fmt, char groupingSeparator, char decimalSeparator) {
		try {
			getDecimalFormat(fmt, groupingSeparator, decimalSeparator).parse(num);
		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
	
	public static boolean valid(String num, String fmt) {
		return valid(num, fmt, '.', ',');
	}

	public static float parseFloat(String num, String fmt, char groupingSeparator, char decimalSeparator) {
		return (float) parse(num, fmt, groupingSeparator, decimalSeparator);
	}

	public static float parseFloat(String num, String fmt) {
		return parseFloat(num, fmt, '.', ',');
	}

	public static long parseLong(String num, String fmt, char groupingSeparator, char decimalSeparator) {
		return (long) parse(num, fmt, groupingSeparator, decimalSeparator);
	}

	public static long parseLong(String num, String fmt) {
		return parseLong(num, fmt, '.', ',');
	}

	public static int parseInt(String num, String fmt, char groupingSeparator, char decimalSeparator) {
		return (int) parse(num, fmt, groupingSeparator, decimalSeparator);
	}

	public static int parseInt(String num, String fmt) {
		return parseInt(num, fmt, '.', ',');
	}

	public static short parseShort(String num, String fmt, char groupingSeparator, char decimalSeparator) {
		return (short) parse(num, fmt, groupingSeparator, decimalSeparator);
	}

	public static short parseShort(String num, String fmt) {
		return parseShort(num, fmt, '.', ',');
	}

	public static byte parseByte(String num, String fmt, char groupingSeparator, char decimalSeparator) {
		return (byte) parse(num, fmt, groupingSeparator, decimalSeparator);
	}

	public static byte parseByte(String num, String fmt) {
		return parseByte(num, fmt, '.', ',');
	}

}
