package br.org.emilio.utilitarios.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StringUtil {

	private StringUtil() {
	}
	
	public static boolean empty(String str) {
		if (str == null || "".equals(str.trim())) {
			return true;
		}
		
		return false;
	}
	
	public static String firstUpper(String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	public static String md5(String texto) {  
		MessageDigest md;
		BigInteger hash;
		
		try {
			md = MessageDigest.getInstance("MD5");
			hash = new BigInteger(1, md.digest(texto.getBytes()));  
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		}
		
		return hash.toString(16);              
	}

	public static String onlyLetterNumber(String texto) {
		String novotexto = "";

		for ( int i = 0 ; i < texto.length() ; i++ ) {
			String scarac = texto.substring(i, i + 1);
			char carac = scarac.charAt(0);

			if (carac >= '0' && carac <= '9') {
				novotexto = novotexto + carac;
			} else if (carac >= 'A' && carac <= 'Z') {
				novotexto = novotexto + carac;
			} else if (carac >= 'a' && carac <= 'z') {
				novotexto = novotexto + carac;
			}
		}

		return novotexto;
	}

	public static String onlyNumber(String texto) {
		String novotexto = "";

		for ( int i = 0 ; i < texto.length() ; i++ ) {
			String scarac = texto.substring(i, i + 1);
			char carac = scarac.charAt(0);

			if (carac >= '0' && carac <= '9') {
				novotexto = novotexto + carac;
			}
		}

		return novotexto;
	}

	public static String mask(String texto, String masc) {
		String texto1 = onlyLetterNumber(texto);
		String novotexto = "";

		int posTxt = 0;

		for ( int i = 0 ; i < masc.length() ; i++ ) {
			if (posTxt < texto.length()) {
				String charMasc = masc.substring(i, i + 1);
				String charTxt = texto1.substring(posTxt, posTxt + 1);

				if (charMasc.equals("#")) {
					novotexto = novotexto + charTxt;
					posTxt++;
				} else {
					novotexto = novotexto + charMasc;
				}
			}
		}

		return novotexto;
	}
	
}
