package br.org.emilio.utilitarios.util;

public class CpfCnpjUtil {

	private CpfCnpjUtil() {
	}

	public static boolean validCNPJ(String cnpj) {
		if (cnpj == null || cnpj.length() != 14) {
			return false;
		}

		for ( int i = 0 ; i < 14 ; i++ ) {
			char num = cnpj.charAt(i);
			if (num < '0' || num > '9') {
				return false;
			}
		}

		int d1 = 0;
		int m = 2;

		for ( int i = 11 ; i >= 0 ; i-- ) {
			d1 = d1 + (cnpj.charAt(i) * m);
			m++;
			if (m >= 10)
				m = 2;
		}

		d1 = 11 - (d1 % 11);
		if (d1 >= 10)
			d1 = 0;

		int d2 = d1 * 2;
		m = 3;

		for ( int i = 11 ; i >= 0 ; i-- ) {
			d2 = d2 + (cnpj.charAt(i) * m);
			m++;
			if (m >= 10)
				m = 2;
		}

		d2 = 11 - (d2 % 11);
		if (d2 >= 10)
			d2 = 0;

		String calculado = "" + d1 + d2;
		String digitado = cnpj.substring(12);

		if (!calculado.equals(digitado)) {
			return false;
		}

		return true;
	}

	public static boolean validCPF(String cpf) {
		if (cpf == null || cpf.length() != 11) {
			return false;
		}
		
		return validCNPJ("000" + cpf);
	}
	
	public static String maskCNPJ(String cnpj) {
		return StringUtil.mask(cnpj, "##.###.###/####-##");
	}
	
	public static String maskCPF(String cpf) {
		return StringUtil.mask(cpf, "###.###.###-##");
	}
	
}
