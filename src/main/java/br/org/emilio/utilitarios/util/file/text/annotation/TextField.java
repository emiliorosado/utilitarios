package br.org.emilio.utilitarios.util.file.text.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TextField {
	int length();
	int decimals() default 0;
	String dateFormat() default "yyyy-MM-dd";
	String trueValue() default "true";
	String falseValue() default "false";
}
