package br.org.emilio.utilitarios.util;

public class EmailUtil {

	private EmailUtil() {
	}

	public static boolean validEmail(String email) {
		if (email == null || email.length() == 0) {
			return false;
		}

		if (email.charAt(0) == '@' || email.charAt(email.length() - 1) == '@') {
			return false;
		}

		int arroba = 0;

		for ( int i = 0 ; i < email.length() ; i++ ) {
			char carac = email.charAt(i);
			int codi = carac;

			if (carac == '@') {
				arroba++;
			} else if (codi < 33 || codi > 122) {
				return false;
			} else if (carac == '\'' || carac == '"' || carac == '`' || carac == '<' ||
					carac == '>' || carac == '\\' || carac == '&') {
				return false;
			}
		}

		if (arroba != 1) {
			return false;
		}

		return true;
	}

}
