package br.org.emilio.utilitarios.util;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	private DateUtil() {
	}
	
	public static Date truncate(Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}
	
	public static Date setMaxTime(Date dt) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		
		return cal.getTime();
	}
	
	public static Date newDate() {
		return truncate(new Date());
	}
	
	public static Date newDate(int year, int month, int day) {
		DecimalFormat df = newNumberFormat("00");
		DecimalFormat dfy= newNumberFormat("0000");
		String dt = dfy.format(year) + "-" + df.format(month) + "-" + df.format(day);
		
		return truncate(parse(dt, "yyyy-MM-dd"));
	}
	
	private static DecimalFormat newNumberFormat(String fmt) {
		return new DecimalFormat(fmt);
	}
	
	private static SimpleDateFormat newDateFormat(String fmt) {
		SimpleDateFormat sdf = new SimpleDateFormat(fmt);
		sdf.setLenient(false);
		return sdf;
	}
	
	public static Date lessDate(Date dt1, Date dt2) {
		if (truncate(dt1).getTime() < truncate(dt2).getTime()) {
			return new Date(dt1.getTime());
		}
		
		return new Date(dt2.getTime());
	}
	
	public static Date greaterDate(Date dt1, Date dt2) {
		if (truncate(dt1).getTime() > truncate(dt2).getTime()) {
			return new Date(dt1.getTime());
		}
		
		return new Date(dt2.getTime());
	}
	
	public static Date parse(String date, String fmt) {
		try {
			return newDateFormat(fmt).parse(date);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static String format(Date date, String fmt) {
		return newDateFormat(fmt).format(date);
	}
	
	public static boolean valid(String date, String fmt) {
		try {
			newDateFormat(fmt).parse(date);
		} catch (ParseException e) {
			return false;
		}
		
		return true;
	}
	
	public static Date firstDate(Date date) {
		int mes = month(date);
		int ano = year(date);

		return newDate(ano, mes, 1);
	}
	
	public static Date lastDate(Date date) {
		int mes = month(date);
		int ano = year(date);

		return newDate(ano, mes, lastDay(date));
	}
	
	public static int lastDay(Date date) {
		int mes = month(date);
		int ano = year(date);

		if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
			return 31;
		else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
			return 30;
		else if ((ano % 4) == 0)
			return 29;

		return 28;
	}

	public static int day(Date date) {
		return Integer.parseInt(format(date, "dd"), 10);
	}

	public static int month(Date date) {
		return Integer.parseInt(format(date, "MM"), 10);
	}

	public static String monthName(Date date) {
		int mes = month(date);
		String[] meses = {
			"janeiro", "fevereiro", "março", "abril", "maio", "junho",
			"julho", "agosto", "setembro", "outubro", "novembro", "dezembro"};
		
		return meses[mes - 1];
	}

	public static int year(Date date) {
		return Integer.parseInt(format(date, "yyyy"), 10);
	}

	public static int weekDay(Date date) {
		return Integer.parseInt(format(date, "u"), 10);
	}

	public static String weekDayName(Date date) {
		int dia = weekDay(date);
		String[] semana = {
			"segunda", "terça", "quarta", "quinta", "sexta", "sábado", "domingo"};
		
		return semana[dia - 1];
	}

	public static Date minDate() {
		return truncate(parse("0001-01-01", "yyyy-MM-dd"));
	}

	public static Date maxDate() {
		return setMaxTime(parse("9999-12-31", "yyyy-MM-dd"));
	}
	
	public static Date addDays(Date dt, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		
		cal.add(Calendar.DAY_OF_MONTH, days);
		
		return cal.getTime();
	}
	
	public static Date addMonths(Date dt, int months) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		
		cal.add(Calendar.MONTH, months);
		
		return cal.getTime();
	}
	
	public static Date addYears(Date dt, int years) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(dt);
		
		cal.add(Calendar.YEAR, years);
		
		return cal.getTime();
	}
	
	private static LocalDate getLocalDate(Date dt) {
		return LocalDate.of(year(dt), month(dt), day(dt));
	}
	
	public static long diffDays(Date dt1, Date dt2) {
		long tm1 = truncate(dt1).getTime();
		long tm2 = truncate(dt2).getTime();
		
		return (tm2 - tm1) / 1000 / 60 / 60 / 24;
	}
	
	public static int diffMonths(Date dt1, Date dt2) {
		LocalDate ld1 = getLocalDate(dt1);
		LocalDate ld2 = getLocalDate(dt2);
		
		Period per = Period.between(ld1, ld2);
		int months = per.getMonths();
		int years = per.getYears();
		
		return months + (years * 12);
	}
	
	public static int diffYears(Date dt1, Date dt2) {
		LocalDate ld1 = getLocalDate(dt1);
		LocalDate ld2 = getLocalDate(dt2);
		
		return Period.between(ld1, ld2).getYears();
	}

}
