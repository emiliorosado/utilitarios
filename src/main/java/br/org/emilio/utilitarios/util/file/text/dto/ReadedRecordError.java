package br.org.emilio.utilitarios.util.file.text.dto;

import lombok.Data;

@Data
public class ReadedRecordError {

	private int column;
	private String field;
	private String value;
	private String description;
	
}
