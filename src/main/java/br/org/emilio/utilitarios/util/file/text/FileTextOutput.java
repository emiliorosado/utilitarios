package br.org.emilio.utilitarios.util.file.text;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class FileTextOutput {
	
	private OutputStream outputStream;
	private OutputStreamWriter outputStreamReader;
	private BufferedWriter bufferedWriter;
	private RecordTextConverter converter;

	public FileTextOutput(String pathname) {
		try {
			File f = new File(pathname);
			this.outputStream = new FileOutputStream(f);
			this.outputStreamReader = new OutputStreamWriter(outputStream);
			this.bufferedWriter = new BufferedWriter(outputStreamReader);
			this.converter = new RecordTextConverter();
		} catch (FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void close() {
		try {
			this.bufferedWriter.close();
			this.outputStreamReader.close();
			this.outputStream.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void write(String line) {
		try {
			this.bufferedWriter.write(line);
			this.bufferedWriter.newLine();
			this.bufferedWriter.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void write(Object object) {
		this.write(this.converter.toText(object));
	}
	
}
